



// 1
const btn = document.getElementById("btn");

btn.addEventListener("click", () => {
  //vid klick ändra färg - dvs lägg till css klass
  btn.classList.add("color");
});

// 2
const text = document.querySelector(".text");
const changeButton = document.querySelector(".to-change");

changeButton.addEventListener("click", () => {
  text.textContent = "Helt ny text";
});

// 3
const image = document.getElementById("image");
const imageBtn = document.getElementById("imageBtn");
let isCow = true;

imageBtn.addEventListener("click", () => {
  if (isCow) {
    image.src = "images/chicken.png";
  } else {
    image.src = "images/cow.png"
  }
  isCow = !isCow
});


// 4

const myDiv = document.querySelector(".bg-color")

myDiv.addEventListener("mouseover", function(){
    myDiv.classList.add("color");
})


// 5

const textContainer = document.getElementById("textContainer")
const createBtn = document.getElementById("createText")


createBtn.addEventListener("click", () => {
    const spanElement = document.createElement("span")
    spanElement.textContent = "helt nytt element"
    spanElement.classList.add("text-styling")
   textContainer.appendChild(spanElement)
})


// 6

const list = document.getElementById("fruitsList")
const fruits = ["äpple", "banan", "kiwi", "jordgubbe"]


for (let fruit of fruits){
    const listItem = document.createElement("li")
    listItem.textContent = fruit
    list.appendChild(listItem)
}