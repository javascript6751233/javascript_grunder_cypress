function showNotes() {
    if (window.location.pathname.includes("index.html")) {
        const noteList = document.querySelector('#note-list');
        Object.keys(localStorage).forEach(noteId => {
            const noteText = localStorage.getItem(noteId);
            const listItem = document.createElement('li');
            const anchor = document.createElement('a');
            anchor.href = "note.html?id=" + noteId;
            anchor.textContent = noteText;
            listItem.appendChild(anchor);
            listItem.classList.add('note-item');
            noteList.appendChild(listItem);
        });
    }
}

document.addEventListener('DOMContentLoaded', function () {
    const saveNoteBtn = document.querySelector('#save-note-btn');
    const editNoteBtn = document.querySelector('#edit-note-btn');
    const deleteNoteBtn = document.querySelector('#delete-note-btn');
    const clearNotesBtn = document.querySelector('#clear-notes-btn');

    if (saveNoteBtn) {
        saveNoteBtn.addEventListener('click', saveNote);
    }

    if (editNoteBtn) {
        editNoteBtn.addEventListener('click', editNote);
    }

    if (deleteNoteBtn) {
        deleteNoteBtn.addEventListener('click', deleteNote);
    }

    if (clearNotesBtn) {
        clearNotesBtn.addEventListener('click', clearNotesList);
    }
});

function saveNote() {
    const noteText = document.querySelector('#note-text').value;
    if(noteText !== "") {
        const noteId = generateId();
        localStorage.setItem(noteId, noteText);
        window.location.href = 'index.html';
    }
}

function generateId() {
    return 'note-' + crypto.randomUUID();
}

function showNoteContent() {
    if (window.location.pathname.includes("note.html")) {
        const urlParams = new URLSearchParams(window.location.search);
        const noteId = urlParams.get('id');
        console.log('Note ID:', noteId);
        const noteText = localStorage.getItem(noteId);
        const noteContent = document.querySelector('#note-content');
        noteContent.innerText = noteText;
    }
}

function editNote() {
    const noteId = window.location.search.substring(4);
    const noteText = localStorage.getItem(noteId);
    const updatedNoteText = prompt('Redigera anteckning:', noteText);
    if (updatedNoteText !== null) {
        localStorage.setItem(noteId, updatedNoteText);
        window.location.reload();
    }
}

function deleteNote() {
    const noteId = window.location.search.substring(4);
    const confirmDelete = confirm('Är du säker på att du vill ta bort anteckningen?');
    if (confirmDelete) {
        localStorage.removeItem(noteId);
        window.location.href = 'index.html';
    }
}

function clearNotesList() {
    const noteList = document.querySelector('#note-list');
    noteList.innerHTML = '';
    localStorage.clear();
}

function showNew() {
    if (window.location.pathname.includes("new.html")) {
        const newNote = document.querySelector('#note-text');
        newNote.focus();
    }
}

showNotes();

showNoteContent();

showNew();
