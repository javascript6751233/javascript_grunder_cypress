//Ordvändare
const wordSwitcherDiv = document.getElementById("Ordvändare");
const textInputElement = document.getElementById("text input");
const outputElement = document.createElement("h3");

wordSwitcherDiv.appendChild(outputElement);

textInputElement.addEventListener("input", function () {
  let inputString = textInputElement.value;

  const stringArray = inputString.split(" ");
  stringArray.reverse();

  outputElement.innerText = stringArray.join(" ");
});

//Vokalkontroll
const vowelControlDiv = document.getElementById("Vokalkontroll");
const vowelInputElement = document.getElementById("vowel input");

const vowelOutputElement = document.createElement("h3");
const vowelCountOutputElement = document.createElement("h3");
vowelControlDiv.appendChild(vowelOutputElement);
vowelControlDiv.appendChild(vowelCountOutputElement);

vowelInputElement.addEventListener("input", function () {
  let vowelOutputString = "";
  let vowelCount = 0;
  let vowelInputString = vowelInputElement.value;
  let vowelString = "aeiouyåäö";

  for (let index = 0; index < vowelInputString.length; index++) {
    let character = vowelInputString.charAt(index);

    if (vowelString.includes(character)) {
      if (!vowelOutputString.includes(character)) {
        vowelOutputString += character;
        vowelOutputElement.innerText = vowelOutputString;
      }
      vowelCount++;
      vowelCountOutputElement.innerText = vowelCount;
    }
  }
});

//Intitialförkortare
const initalsDiv = document.getElementById("Initialförkortare");
const nameInputElement = document.getElementById("name input");

const initialsOutputElement = document.createElement("h3");
initalsDiv.appendChild(initialsOutputElement);

nameInputElement.addEventListener("input", function () {
  let nameString = nameInputElement.value;
  const nameArray = nameString.split(" ");
  let initialsOutputString = "";

  for (let i = 0; i < nameArray.length; i++) {
    initialsOutputString += nameArray[i].charAt(0);
  }

  initialsOutputElement.innerText = "Initialer: " + initialsOutputString;
});

//Telefonformaterare
const phoneDiv = document.getElementById("Telefonformaterare");
const phoneInputElement = document.getElementById("phone input");

const phoneOutputElement = document.createElement("h3");
phoneDiv.appendChild(phoneOutputElement);

phoneInputElement.addEventListener("input", function () {
  let phoneString = phoneInputElement.value;
  let first3 = phoneString.slice(0, 3);
  let second3 = phoneString.slice(3, 6);
  let third2 = phoneString.slice(6, 8);
  let remaining = phoneString.slice(8, phoneString.length + 1);

  phoneOutputElement.innerText =
    first3 + "-" + second3 + " " + third2 + " " + remaining;
});

//Validering av input-data
const form = document.getElementById("form");
const nameInput = document.getElementById("name");
const emailInput = document.getElementById("email");
const passwordInput = document.getElementById("password");
const validationMessage = document.getElementById("validation");
const btn = document.getElementById("btn");

nameInput.addEventListener("input", formValidation);
emailInput.addEventListener("input", formValidation);
passwordInput.addEventListener("input", formValidation);

function formValidation() {
  const password = passwordInput.value;
  const email = emailInput.value;
  const name = nameInput.value;

  if (password && email && name) {
    validationMessage.textContent = "";
    btn.disabled = false;
  } else {
    validationMessage.textContent = "Alla fält måste vara ifyllda";
  }
}
