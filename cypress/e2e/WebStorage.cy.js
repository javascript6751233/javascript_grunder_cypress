/// <reference types="cypress"/>

beforeEach(() => {
    cy.visit(`localhost:${Cypress.env("SERVER_PORT")}/src/WebStorage/`);
    window.localStorage.clear();
});

describe("DOM Manipulation part 1 tests", () => {

    it("should save a note", () => {
        cy.get('#new-note').click();
        cy.get("#note-text").type("Test note {enter}")
        cy.get("#save-note-btn").click()
        cy.url().should('include', 'index.html');
        cy.get('#note-list').should('contain', 'Test note');
    });

    it("should not save note when text is empty", () => {
        cy.get('#new-note').click();
        cy.get("#save-note-btn").click();
        cy.url().should('not.include', 'index.html');
    });

    it("should edit a note", () => {
        const expected = "Edited note"
        cy.get('#new-note').click();
        cy.get("#note-text").type("Test note {enter}")
        cy.get("#save-note-btn").click()
        cy.url().should('include', 'index.html');
        cy.get('.note-item').click();
        cy.window().then(win => cy.stub(win, 'prompt').returns(expected));
        cy.get("#edit-note-btn").click()
        cy.get("#go-back").click()
        cy.get('#note-list').should('contain', expected);
    });

    it("should delete a note", () => {
        cy.get('#new-note').click();
        cy.get("#note-text").type("Test note {enter}")
        cy.get("#save-note-btn").click()
        cy.get('.note-item').click();
        cy.get('#delete-note-btn').click();
    });

    it("should clear all notes", () => {
        cy.get('#new-note').click();
        cy.get("#note-text").type("Test note1 {enter}")
        cy.get("#save-note-btn").click()
        cy.get('#new-note').click();
        cy.get("#note-text").type("Test note2 {enter}")
        cy.get("#save-note-btn").click()
        cy.get('#clear-notes-btn').click()
        cy.get('.note-item').should('not.exist');
    });

});
