/// <reference types="cypress"/>

const testIp = "127.0.0.1"
const testCountry = "country_stub"
const testCity = "city_stub"
const testIsp = "ISP_stub"
const testLat = 54.8888
const testLon = 12.0410
const testTemp = "temp_stub"

beforeEach(() => {
    cy.visit(`http://localhost:${Cypress.env("SERVER_PORT")}/src/fetch/Bonus_API/`);
});


describe("API:er - Bonusnivå", () => {

    it('4. Bonus - Should display fetched ip location weather and map', () => {

        cy.intercept("https://api.ipify.org/?format=json", { ip: testIp })
        cy.intercept(`http://ip-api.com/json/${testIp}`, {
            country: testCountry,
            city: testCity,
            isp: testIsp,
            lat: testLat,
            lon: testLon
        })
        cy.intercept(`https://api.met.no/weatherapi/locationforecast/2.0/compact?lat=${testLat}&lon=${testLon}`, { properties: { timeseries: [{ data: { instant: { details: { air_temperature: testTemp } } } }] } })

        cy.get('[data-testid="ip-btn"]').click()

        cy.get('[data-testid="ip-weather"]')
            .should('contain', testTemp)
            .should('be.visible')

        cy.get('[data-testid="ip-map"]')
            .invoke('attr', 'src')
            .should('contain', testLon)
    })

    it('5. Bonus - Should display alert when status code is not in the 200 range', () => {

        cy.intercept("https://api.quotable.io/random", { statusCode: 403 })

        const stub = cy.stub()
        cy.on('window:alert', stub)

        cy.get('[data-testid="random-quote-btn"]').click()
            .then(() => {
                expect(stub).to.have.been.calledOnce
            })
    })

    it('5. Bonus - Should display alert on network error', () => {

        cy.intercept("https://api.quotable.io/random", { forceNetworkError: true })

        const stub = cy.stub()
        cy.on('window:alert', stub)

        cy.get('[data-testid="random-quote-btn"]').click()
            .then(() => {
                expect(stub).to.have.been.calledOnce
            })
    })
})

